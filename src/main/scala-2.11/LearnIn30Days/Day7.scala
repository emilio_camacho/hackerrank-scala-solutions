package LearnIn30Days

/**
  * Created by ecamacho on 8/10/16.
  */
class Day7 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    println(((lines last) split (" ") reverse) mkString(" "))
  }
}
