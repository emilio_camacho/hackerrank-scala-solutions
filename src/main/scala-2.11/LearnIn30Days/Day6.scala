package LearnIn30Days

/**
  * Created by ecamacho on 8/10/16.
  */
class Day6 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList

    for (string <- lines.tail) {
      breakString(string)
    }

    def breakString(string: String): Unit = {
      val even = string.indices filter ( _ % 2 == 0) map (string.charAt) mkString("")
      val odd = string.indices filter ( _ % 2 != 0) map (string.charAt) mkString("")
      println(even concat (" ") concat odd)
    }
  }
}
