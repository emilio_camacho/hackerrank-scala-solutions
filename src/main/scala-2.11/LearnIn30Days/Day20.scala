package LearnIn30Days

/**
  * Created by ecamacho on 8/23/16.
  */
class Day20 {
  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    var n = sc.nextInt();
    var unsorted = new Array[Int](n);
    for(a_i <- 0 to n-1) {
      unsorted(a_i) = sc.nextInt();
    }

    var sorted = unsorted.sorted

    var numberOfSwaps = 0

    while (unsorted.deep != sorted.deep) {
      for ( i <- 0 until (unsorted.size - 1)) {
        if (unsorted(i) > unsorted(i + 1) ) {
          val aux = unsorted(i + 1)
          unsorted(i + 1) = unsorted(i)
          unsorted(i) = aux
          numberOfSwaps += 1
        }
      }
    }

    println(s"Array is sorted in ${numberOfSwaps} swaps.")
    println(s"First Element: ${unsorted head}")
    println(s"Last Element: ${unsorted last}")

  }
}
