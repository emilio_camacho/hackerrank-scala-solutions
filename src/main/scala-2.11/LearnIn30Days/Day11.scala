package LearnIn30Days

/**
  * Created by ecamacho on 8/15/16.
  */
class Day11 {
  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    var arr = Array.ofDim[Int](6,6);
    for(arr_i <- 0 to 6-1) {
      for(arr_j <- 0 to 6-1){
        arr(arr_i)(arr_j) = sc.nextInt();
      }
    }
    val results = for ( i <- 0 until 4;
                        j <- 0 until 4) yield {
      arr(i)(j) + arr(i)(j + 1) + arr(i)(j + 2) + arr(i+1)(j+1) + arr(i+2)(j) + arr(i+2)(j + 1) + arr(i+2)(j + 2)
    }

    println(results.sorted.reverse(0))
  }
}
