package LearnIn30Days

import java.lang.Integer.toBinaryString
/**
  * Created by ecamacho on 8/15/16.
  */
class Day10 {
  def main(args: Array[String]): Unit = {
    val lines = io.Source.stdin.getLines.toList
    val num = lines.head.toInt

    val arrayNum = toBinaryString(num) split "0" sortBy ( - _.length)
    println(arrayNum(0).length)
  }
}
