package LearnIn30Days

class Calculator
{
  def power(n:Int, p:Int):Int =
    if (p < 0 || n < 0) throw new Exception("n and p should be non-negative")
    else
      p match {
        case pc if pc == 0 => 1
        case _ => n * power(n, p - 1)
      }
}

object Day17 {
  def main(args: Array[String]) {

    var myCalculator=new Calculator();
    var T=scala.io.StdIn.readLine().toInt

    while(T>0){
      val Array(n,p) = scala.io.StdIn.readLine().split(" ").map(_.toInt);
      try{
        var ans=myCalculator.power(n,p);
        println(ans);
      }
      catch{
        case e: Exception => {
          println(e.getMessage());
        }
      }
      T-=1;
    }
  }
}
