package LearnIn30Days

/**
  * Created by ecamacho on 8/12/16.
  */
class Day9 {
  def main(args: Array[String]): Unit = {
    val lines = io.Source.stdin.getLines.toList
    val num = lines.head.toInt
    def Factorial(num:Int): Int =
      num match {
        case n if n <= 0 => 1
        case _ => num * Factorial(num -1)
      }

    println(Factorial(num))
  }
}
