package LearnIn30Days

/**
  * Created by ecamacho on 8/11/16.
  */
class Day8 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    val numberCases = lines.head.toInt
    val book = lines drop 1 take numberCases
    val searches = lines drop 1 drop numberCases take numberCases
    val mapBook = (for (entry <- book) yield (entry.split(" ")(0) -> entry.split(" ")(1))) toMap

    for (search <- searches) {
      if (mapBook.keySet contains search) println(search + "=" + mapBook.get(search).get)
      else println("Not found")
    }
  }
}
