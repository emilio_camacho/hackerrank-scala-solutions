/*
val r = (1 until 1000000000).view.filter(n => n % 3 == 0 || n % 5 == 0).sum
*/

val lines = List("1", "1000000000")
for (num <- lines.tail) {
  val numCalculate = BigInt(num) - 1
  println(calculate(5)(numCalculate) + calculate(3)(numCalculate) - calculate(15)(numCalculate))
}

def calculate(divisor:BigInt)(number:BigInt): BigInt =
{
  val numDivisors = number / divisor
  (numDivisors * (numDivisors + 1))/2 * divisor
}