package ProjectEuler

/**
  * Created by ecamacho on 8/22/16.
  */
class Eular015 {
  def main(args: Array[String]) {

    val lines = io.Source.stdin.getLines.toList
    val triangle = new Array[Array[BigInt]](1001)

    for (i <- 0 to 1000) {
      val auxArray = new Array[BigInt](i + 1)
      for (value <- 0 to i) {
        auxArray(value) = value match {
          case v if v == 0 || v == i => 1
          case _ =>   triangle(i-1)(value-1) + triangle(i-1)(value)
        }
      }
      triangle(i) = auxArray
    }

    for (Array(rows, cols) <- lines.tail.map(_.split(" ").map(_.toInt))) {
      println(triangle(rows+cols)(rows) % 1000000007)
    }
  }
}
