package ProjectEuler

/**
  * Created by ecamacho on 8/15/16.
  */
class Eular012 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList

    def infiniteStream(n:Int): Stream[Int] = n #:: infiniteStream(n+1)
    def primes(stream: Stream[Int]): Stream[Int] = stream.head #:: primes(stream filter (_ % stream.head != 0))
    val allPrimes = primes(infiniteStream(2)) take 25 toList
    var allDivisors:Map[Int,List[Int]] = Map()

    def infiniteTrianglesStream(num:Int = 1, iteration:Int = 1): Stream[Int] =
      iteration match {
        case i if i == 1 => 1 #:: infiniteTrianglesStream(1, 2)
        case _ => (num + iteration) #:: infiniteTrianglesStream(num + iteration, iteration + 1)
      }

    val triangles = infiniteTrianglesStream()

    def getAllDivisors(num:Int, currentPrime:Int = 2): List[Int] =
      num match {
        case n if n == 1 || currentPrime > num => Nil
        case n if n % currentPrime == 0 => {
          allDivisors.get(num / currentPrime) match {
            case Some(value) => {
              currentPrime :: value
            }
            case None => {
              val result = currentPrime :: getAllDivisors(num / currentPrime, currentPrime)
              allDivisors += num -> result
              result
            }
          }

        }
        case _ => {
          val nextPrimes = allPrimes dropWhile(_ <= currentPrime)
          if (nextPrimes.isEmpty) Nil
          else getAllDivisors(num, nextPrimes head)
        }
      }

    def getNumDivisors(num:Int):Int = {
      num match {
        case n if n ==1 => 1
        case _ => {
          val divisorsForNum = getAllDivisors(num)
          allDivisors += num -> divisorsForNum
          val groupDivisors =  divisorsForNum groupBy (n => n)
          val numDivisors = (for (divisor <- groupDivisors) yield ((divisor._2 size) + 1)).product
          numDivisors
        }
      }
    }

    for (num <- lines.tail map (_.toInt)) {
      println(triangles dropWhile (getNumDivisors(_) <= num) head)
    }

  }
}
