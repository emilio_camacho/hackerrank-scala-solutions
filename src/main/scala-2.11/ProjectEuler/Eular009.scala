package ProjectEuler

/**
  * Created by ecamacho on 8/11/16.
  */
class Eular009 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    for (line <- lines.tail) {
      val num = line.toInt
      val result = for( a <- 1 to num / 3;
                        b <- List( (((num-a)+((a*a)/(a-num)))/2));
                        c <- List(num - b - a)
                        if a + b + c == num &&  (Math.pow(a, 2) + Math.pow(b,2)) == Math.pow(c, 2))
        yield {
          a * b * c
        }
      if (result.isEmpty) println("-1")
      else println(result.max)
    }
  }
}
