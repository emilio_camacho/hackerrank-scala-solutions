package ProjectEuler

/**
  * Created by ecamacho on 8/23/16.
  */
class Eular016 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    for (line <- lines.tail.map(_.toInt)) {
      println(BigInt(2).pow(line).toString.map(_.asDigit).sum)
    }
  }
}
