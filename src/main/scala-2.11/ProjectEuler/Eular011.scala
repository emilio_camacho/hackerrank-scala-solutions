package ProjectEuler

/**
  * Created by ecamacho on 8/15/16.
  */
class Eular011 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    val numbers = for (line <- lines;
                       number <- line split(" ") map (BigInt(_)))
      yield {
        number
      }
    val limit:Int = 20
    val numElements:Int = 4

    def productRow(numElements:Int)(limit:Int)(elements:List[BigInt], start:Int, incRow:Int, incCol:Int):Option[BigInt] = {
      val row = start % limit
      val col = start / limit
      val toRow = row + (incCol * (numElements - 1))
      val toCol = col + (incRow * (numElements - 1))

      if ( (0 until limit contains toRow) && (0 until limit contains toCol) ){
        Some((for (i <- 0 until numElements) yield {
          elements(start + (incRow * i * limit) + (incCol * i))
        }).product)
      } else None
    }

    val getProduct20x20 = productRow(numElements)(limit)_

    val result = (for (i <- 0 until limit * limit)
      yield {
        List(getProduct20x20(numbers, i, 0 , 1),
          getProduct20x20(numbers, i, 1 , 0),
          getProduct20x20(numbers, i, 1 , 1),
          getProduct20x20(numbers, i, 1 , -1)
        ) max
      }) max

    println(result.getOrElse("Result not found"))
  }
}
