package ProjectEuler

/**
  * Created by ecamacho on 8/11/16.
  */
class Eular008 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    val numUseCases = lines.head.toInt
    for (i <- 1 to numUseCases) {
      val useCase = lines.tail drop ((i - 1) * 2) take 2
      val numElements = useCase.head.split(" ")(1).toInt
      val numbers = useCase.last.map( num => BigInt(num.toString)).toArray

      println(findMaxProduct(numbers.reverse, 0, numElements))
    }
    def findMaxProduct(numbers:Array[BigInt], pos:Int, numElements:Int, max:BigInt = 0):BigInt = {
      if ( pos > numbers.size) {
        max
      } else {
        val from = if (pos + numElements > numbers.size) pos - numElements else pos
        val product =(numbers drop from take numElements).product
        if (product > max) findMaxProduct(numbers, pos + 1, numElements, product)
        else findMaxProduct(numbers, pos + 1, numElements, max)
      }
    }
  }
}
