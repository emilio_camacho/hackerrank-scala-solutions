package ProjectEuler

/**
  * Created by ecamacho on 8/18/16.
  */
object Eular014 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    val maxStorage = (lines.tail.map(_.toInt)) max
    val historical = new Array[Int](maxStorage)
    def nextNumber(num:BigInt):BigInt =
      num match {
        case n if n % 2 == 0 => n / 2
        case n => 3 * n + 1
      }

    def getIterations(num:BigInt): Int =
      num match {
        case n if n < maxStorage =>
          historical(n.toInt - 1) match {
            case found:Int if found > 0 => found
            case _ => num match {
              case n if n == 1 => 1
              case _ =>{
                var newResult = 1 + getIterations(nextNumber(num))
                historical(n.toInt - 1) = newResult
                newResult
              }
            }
          }
        case _ =>  1 + getIterations(nextNumber(num))
      }

    for (i <- 1 to maxStorage) getIterations(i)
    val historicalByIndex = new Array[Int](maxStorage)
    for (i <- 0 until maxStorage) {
      i match {
        case n if n != 0 => {
          if (historical(i-1) > historical(i)){
            historical(i) = historical(i-1)
            historicalByIndex(i) = historicalByIndex(i - 1)
          } else {
            historicalByIndex(i) = i
          }
        }
        case _ => historicalByIndex(0) = 0
      }
    }

    for ( line <- lines.tail.map(_.toInt - 1) ) {
      println(historicalByIndex(line) + 1)
    }
  }
}
