package ProjectEuler

/**
  * Created by ecamacho on 8/10/16.
  */
class Eular002 {
  def main(args: Array[String]) {
    def createFibonassy(previus: BigInt, n: BigInt): Stream[BigInt] = (previus + n) #:: createFibonassy(n, previus + n)

    val lines = io.Source.stdin.getLines.toList

    val fibo = createFibonassy(1, 1)
    for (value <- lines tail) {
      println(fibo filter (_ % 2 == 0) takeWhile (_ < BigInt(value))  sum)
    }

  }

}
