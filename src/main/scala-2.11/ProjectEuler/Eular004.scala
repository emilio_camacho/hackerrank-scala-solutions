package ProjectEuler

/**
  * Created by ecamacho on 8/10/16.
  */
class Eular004 {
  def main(args: Array[String]) {

    def getAllPalindromic(): Vector[Int] = {
      (((for (i <- 100 to 999;
              b <- i to 999 if (i * b).toString == (i*b).toString.reverse ) yield i * b) toVector) distinct) sorted
    }

    val palindromic = getAllPalindromic()

    val lines = io.Source.stdin.getLines.toList.tail

    for (number <- lines) {
      println(palindromic filter (_ < number.toInt) last)
    }

  }

}
