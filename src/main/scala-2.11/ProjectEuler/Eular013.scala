package ProjectEuler

/**
  * Created by ecamacho on 8/18/16.
  */
class Eular013 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    println(lines.tail.map(BigInt(_)).foldLeft(BigInt(0))(_ + _).toString take 10)
  }
}
