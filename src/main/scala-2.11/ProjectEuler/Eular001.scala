package ProjectEuler

/**
  * Created by ecamacho on 8/10/16.
  */
class Eular001 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    for (num <- lines.tail) {
      val numCalculate = BigInt(num) - 1
      println(calculate(5)(numCalculate) + calculate(3)(numCalculate) - calculate(15)(numCalculate))
    }

    def calculate(divisor:BigInt)(number:BigInt): BigInt =
    {
      val numDivisors = number / divisor
      (numDivisors * (numDivisors + 1))/2 * divisor
    }
  }
}
