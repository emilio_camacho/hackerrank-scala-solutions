package ProjectEuler

/**
  * Created by ecamacho on 8/12/16.
  */
class Eular010 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    def calculatePrimes(maxNum:Int)(num:Int)(primes:Vector[Int]): Vector[Int] =
    {
      num match {
        case n if n == maxNum => primes
        case n if primes exists (num % _ == 0) => calculatePrimes(maxNum)(num + 1)(primes)
        case _ => calculatePrimes(maxNum)(num + 1)(primes :+ num)
      }

    }
    val maxValue = (lines map (_.toInt)) max
    val allPrimes = (calculatePrimes(maxValue)(2)(Vector())) map (BigInt(_))
    for (line <- lines.tail) {
      val num = line.toInt
      println((allPrimes takeWhile( _ <= num)).fold(BigInt(0))(_ + _))
    }
  }
}
