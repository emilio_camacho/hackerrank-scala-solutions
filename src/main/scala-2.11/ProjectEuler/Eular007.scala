package ProjectEuler

/**
  * Created by ecamacho on 8/11/16.
  */
class Eular007 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList.tail
    def calculatePrimes(numPrimes:Int)(num:BigInt)(primes:Vector[BigInt]): Vector[BigInt] =
    {
      if (primes.size == numPrimes) primes
      else {
        if (primes exists (num % _ == 0)) {
          calculatePrimes(numPrimes)(num + 1)(primes)
        } else {
          calculatePrimes(numPrimes)(num + 1)(primes :+ num)
        }
      }
    }
    val maxValue = (lines map (_.toInt)) max

    val allPrimes = calculatePrimes(maxValue)(2)(Vector())

    for (line <- lines) {
      println(allPrimes(line.toInt - 1))
    }
  }
}
