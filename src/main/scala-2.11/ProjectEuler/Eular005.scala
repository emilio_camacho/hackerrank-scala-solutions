package ProjectEuler

/**
  * Created by ecamacho on 8/11/16.
  */
class Eular005 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList.tail

    for (line <- lines) {
      val num = BigInt(line)
      val range = 2 to num.toInt

      def maximunNum(num: BigInt): BigInt =
        if (range forall (num % _ == 0)) {num}
        else maximunNum( num + 1)

      println(maximunNum(num))

    }
  }
}
