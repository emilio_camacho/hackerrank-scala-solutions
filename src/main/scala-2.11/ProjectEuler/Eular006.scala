package ProjectEuler

/**
  * Created by ecamacho on 8/11/16.
  */
class Eular006 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList.tail
    for (line <- lines) {
      val num = line.toDouble
      val SquareSum = BigDecimal(Math.pow((num*(num+1))/2, 2));
      val  sumSquares =  BigDecimal((num*(num+1)*((2*num)+1))/6)
      println( (SquareSum - sumSquares).toBigInt.abs )
    }
  }
}
