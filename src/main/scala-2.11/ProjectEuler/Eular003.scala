package ProjectEuler

/**
  * Created by ecamacho on 8/10/16.
  */
class Eular003 {
  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines.toList
    def infiniteStream(n:BigInt): Stream[BigInt] = n #:: infiniteStream(n+1)
    def primes(stream: Stream[BigInt]): Stream[BigInt] = stream.head #:: primes(stream filter (_ % stream.head != 0))
    val allPrimes = primes(infiniteStream(2))
    def isPrime(num:BigInt):Boolean = {
      allPrimes takeWhile (_ <= (Math.sqrt(num.toDouble).toInt + 1) ) forall (num % _ != 0)
    }

    for (value <- lines.tail map (sval => BigInt(sval))) {
      if (isPrime(value)) {
        println(value)
      } else {
        val bigVal = reduceValue(value)
        println( (allPrimes takeWhile (_ <= value) filter  (value % _ == 0) toList) last )
      }
    }

    def reduceValue(num:BigInt): BigInt =
      num match {
        case n if n == 1 => 2
        case n if n == 2 => 2
        case n if n % 2 == 0 => reduceValue(num / 2)
        case _ => num
      }
  }
}
